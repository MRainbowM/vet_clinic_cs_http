import json 
from flask import Flask, request, render_template, flash, redirect, json
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired

import hashlib
import uuid

from sqlalchemy.dialects.mysql import insert
import sqlalchemy
from sqlalchemy import orm, and_, text, func
import redis
from models import *
import random
import string
import datetime

import win32api
import win32event


POOL = redis.ConnectionPool(host='127.0.0.1', port=6379, db=2)
engine = sqlalchemy.create_engine('mysql+pymysql://maria:1234@localhost/vc_2', pool_size=10, max_overflow=-1)

app = Flask(__name__, template_folder="templates")
app.config['SECRET_KEY'] = 'vet_clinic'

salt = 'dfkgdfkgj'

def logging(ev_name): #запись в журнал
    client_addr = request.remote_addr
    event = f"{client_addr}: '{ev_name}'"
    row = f"{str(datetime.datetime.now())} - {event}\n"
    mutex = WinMutex("mutex_vet_clinic")
    while True:
        mutex.wait_mutex(mutex.mutex)
        event_log = open('event_log.txt', 'a')
        event_log.write(row)
        event_log.close()
        mutex.release()
        return

@app.route('/clear_token',methods=['POST'])
def clear_token():
    logging('clear_token')
    token = request.headers['token']
    Session = sessionmaker(bind=engine)
    session = Session()
    user_q = session.query(User).filter_by(token = token).all()
    user = {}
    for row in user_q:
        user = dict(id = row.id)
    if not user: 
        print("(!)Ошибка")
        return "-1"
 
    update_stmt = user_table.update().values(token = None).where(user_table.c.id == user["id"])
    conn = engine.connect()
    result = conn.execute(update_stmt)
    return "1"

@app.route('/auth',methods=['POST'])
def auth():
    logging('auth')
    login = request.form['login']
    pwd = request.form['password']
    Session = sessionmaker(bind=engine)
    session = Session()
    # user_q = session.query(User).filter_by(username = login, password = pwd).all()
    user_q = session.query(User).filter_by(username = login).all()
    user = {}
    for row in user_q:
        user = dict(id = row.id, password = row.password)
        password = user['password']
        if password !=  hashlib.sha256(salt.encode() + pwd.encode()).hexdigest():
            answer = dict(
            status="error",
            msg="Неверный пароль")
            return answer

    if not user: 
        answer = dict(
            status="error",
            msg="Неверный логин")
        return answer
    token = ""
    
    token = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(30))

    update_stmt = user_table.update().values(token = token).where(user_table.c.id == user["id"])
    conn = engine.connect()
    result = conn.execute(update_stmt)

    answer = dict(
            status="success",
            content=dict(token=token))

    return answer

#CLIENT##############################################################

@app.route('/add_client',methods=['POST'])
def add_client():
    logging('add_client')
    login = request.form['login']
    pwd = request.form['password']

    pwd = hashlib.sha256(salt.encode() + pwd.encode()).hexdigest()

    try:
        insert_stmt = insert(User).values(username=login, password=pwd)
        conn = engine.connect()
        result = conn.execute(insert_stmt)
        user_id = result.inserted_primary_key
        try:
            insert_stmt = insert(Client).values(
            user_id = user_id)
            result = conn.execute(insert_stmt)
            client_id = result.inserted_primary_key
            
            answer = dict(
                status="success",
                content= dict(user_id=user_id, client_id=client_id))
            return answer
        except:
            return {"status":"error","msg":"клиент не создан"}
    except:
        return {"status":"error","msg":"пользователь не создан"}

@app.route('/get_client_by_user',methods=['GET'])
def get_client_by_user(token = ""):
    if token == "":
        token = request.headers['token']
    logging('get_client_by_user')
    Session = sessionmaker(bind=engine)
    session = Session()
    client_q = session.query(Client).join(User, User.id == Client.user_id).filter_by(token = token).all()
    client = {}
    for row in client_q:
        client = dict(
            id = row.id,
            surname = row.surname,
            name = row.name,
            patronymic = row.patronymic,
            phone = row.phone,
            mail = row.mail,
            photo = row.photo,
            date_of_birth = row.date_of_birth,
            user_id = row.user_id)
    if not client: 
        answer = dict(
            status="error",
            msg="клиент не найден")
        return answer
    elif client:
        answer = dict(
            status="success",
            content=dict(client=client))
        return answer

#VISIT###############################################################

@app.route('/get_visit_by_client',methods=['GET'])
def get_visit_by_client():
    logging('get_visit_by_client')
    token = request.headers['token']
    try:
        client = get_client_by_user(token)['content']['client']
    except:
        answer = dict(
            status="error",
            msg="Ошибка авторизации")
        return answer
    Session = sessionmaker(bind=engine)
    session = Session()
    visit_q = session.query(Visit).filter_by(client_id = client['id'])
    visits = {}
    for row in visit_q:
        status = ""
        if row.status == 0:
            status = "актуальный"
        if row.status == 1:
            status = "выполнен"
        if row.status == 2:
            status = "отменен"
        cabinet = get_cabinet(row.cabinet_id)['content']
        doctor = get_worker(row.doctor_id)['content']
        doctor = doctor["surname"] + " " + doctor["name"] + " " + doctor["patronymic"]
        filial = get_filial(row.filial_id)['content']
        filial1 = ""
        filial1 = filial["address"]
        pet = get_pet(token, row.pet_id)['content']
        pet1 = pet["name"]
        cabinet = cabinet["name"]
        visit = dict(
            id = row.id,
            datetime = str(row.date),
            status = status,
            cabinet = cabinet,
            duration = row.duration,
            filial = filial1,
            pet = pet1,
            doctor = doctor)
        key = "visit " + str(row.id)
        visits[key] = visit
    answer = dict(
        status = "success",
        content = visits,
        count = len(visits))
    return answer

@app.route('/cancel_visit',methods=['POST'])
def cancel_visit():
    logging('cancel_visit')
    token = request.headers['token']
    id = request.args.get('visit_id')
    try:
        client = get_client_by_user(token)['content']['client']
    except:
        answer = dict(
            status="error",
            msg="Ошибка авторизации")
        return answer
    try:
        update_stmt = visit_table.update().values(status = 2).where(and_(visit_table.c.id == id, visit_table.c.client_id == client["id"]))
        conn = engine.connect()
        result = conn.execute(update_stmt)
        answer = dict(
            status = "success",
            content = dict(id=str(id)))
        return answer
    except:
        answer = dict(
            status = "error",
            msg = "Ошибка")
        return answer

@app.route('/get_visit_by_pet',methods=['GET']) 
def get_visit_by_pet():
    logging('get_visit_by_pet')
    pet_id = request.args.get('pet_id')
    token = request.headers['token']
    try:
        client = get_client_by_user(token)['content']['client']
    except:
        answer = dict(
            status = "error",
            msg = "Ошибка авторизации")
        return answer
    Session = sessionmaker(bind=engine)
    session = Session()
    visit_q = session.query(Visit).filter_by(client_id = client['id'], pet_id = pet_id, date_of_delete = None)
    visits = {}
    for row in visit_q:
        status = ""
        if row.status == 0:
            status = "актуальный"
        if row.status == 1:
            status = "выполнен"
        if row.status == 2:
            status = "отменен"
        cabinet = get_cabinet(row.cabinet_id)['content']
        doctor = get_worker(row.doctor_id)['content']
        doctor = doctor["surname"] + " " + doctor["name"] + " " + doctor["patronymic"]
        filial = get_filial(row.filial_id)['content']
        filial1 = ""
        filial1 = filial["address"]
        pet = get_pet(token, row.pet_id)['content']
        pet1 = pet["name"]
        cabinet = cabinet["name"]
        visit = dict(
            id = row.id,
            datetime = str(row.date),
            status = status,
            cabinet = cabinet,
            duration = row.duration,
            filial = filial1,
            pet = pet1,
            doctor = doctor)
        key = "visit " + str(row.id)
        visits[key] = visit
    answer = dict(
        status = "success",
        content = visits,
        count = len(visits))
    return answer

@app.route('/add_visit',methods=['POST'])
def add_visit():
    logging('add_visit')
    token = request.headers['token']
    service_id = request.form['service_id']
    worker_id = request.form['worker_id']
    pet_id = request.form['pet_id']
    date = request.form['date']
    filial_id = request.form['filial_id']
    if service_id == '' or worker_id == '' or pet_id == '' or date == '' or filial_id == '':
        answer = dict(
            status="error",
            msg="Не все данные переданы")
        return answer

    try:
        client = get_client_by_user(token)['content']['client']
    except:
        answer = dict(
            status="error",
            msg="Ошибка авторизации")
        return answer
    service = get_service(service_id)['content']
    cost = service["cost"]
    duration = service["duration"]

    try:
        workers = get_workers_by_service(service_id)['content']
    except:
        answer = dict(
            status="error",
            msg="Врачи, оказывающие услугу не найдены")
        return answer  

    worker = "worker " + worker_id

    if not worker in workers:
        answer = dict(
            status="error",
            msg="Ошибка, работник не оказывает данную услугу")
        return answer  

    pet = get_pet(token, pet_id)['content']
    if pet["client"] != client["id"]:
        answer = dict(
            status="error",
            msg="Ошибка, питомец не принадлежит клиенту")
        return answer  
   
    cabinet_id = get_cabinets_by_schedule(filial_id, date, worker_id) 
    if not cabinet_id:
        answer = dict(
            status="error",
            msg="Ошибка, кабинет не найден")
        return answer  
    try:
        insert_stmt = insert(Visit).values(
            date = date,
            cost = cost,
            filial_id = filial_id,
            doctor_id = worker_id,
            pet_id = pet_id,
            client_id = client["id"],
            status = 0,
            duration = duration,
            cabinet_id = cabinet_id)
        conn = engine.connect()
        result = conn.execute(insert_stmt)
        visit_id = result.inserted_primary_key
        answer = dict(
            status = "success",
            content = dict(visit_id = visit_id))
        return answer
    except:
        answer = dict(
            status = "error",
            msg = "Ошибка, прием не создан")
        return answer  
#CABINET##############################################################

@app.route('/get_cabinet',methods=['GET']) 
def get_cabinet(id = ''):
    if id == '':
        id = request.args.get('cabinet_id')

    logging('get_cabinet')
    cache = get_cache("get_cabinet", id)
    try:
        rows = cache["get_cabinet " + str(id)]
        answer = dict(
            status = "success",
            content = rows,
            count = len(rows))
        return answer
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        cabinet_q = session.query(Cabinet).filter_by(id = id).all()
        for row in cabinet_q:
            cabinet = dict(
                id = row.id,
                name = row.name,
                description = row.description,
                filial_id = row.filial_id)
        if not cabinet: 
            answer = dict(
                status = "error",
                msg = "Ошибка, кабинет не найден")
            return answer
        elif cabinet:
            set_cache("get_cabinet " + str(id), cabinet, cache["set"])
            answer = dict(
                status = "success",
                content = cabinet)
            return answer

def get_cabinets_by_schedule(filial_id = '', date = '', worker_id = ''):
    logging('get_cabinets_by_schedule')
    date = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
    cabinet = 0
    # try:
    Session = sessionmaker(bind=engine)
    session = Session()
    schedule_q = session.query(Schedule).filter_by(filial_id = filial_id, worker_id = worker_id).all()
    for row in schedule_q:
        if (row.time1_start != None) and (row.time1_start < date) and (row.time1_end > date):
            return row.cabinet_id
        elif (row.time2_start != None) and (row.time2_start < date) and (row.time2_end > date):
            return row.cabinet_id
        elif (row.time3_start != None) and (row.time3_start < date) and (row.time3_end > date):
            return row.cabinet_id
    return cabinet
#WORKER##############################################################
@app.route('/get_worker',methods=['GET']) 
def get_worker(id = ''):
    if id == '':
       id = request.args.get('worker_id')
    logging('get_worker')
    cache = get_cache("get_worker", id)
    try:
        rows = cache["get_worker " + str(id)]
        answer = dict(
            status = "success",
            content = rows,
            count = len(rows))
        return answer
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        worker_q = session.query(Worker).filter_by(id = id).all()
        for row in worker_q:
            worker = dict(
                id = row.id,
                surname = row.surname,
                name = row.name,
                patronymic = row.patronymic,
                phone = row.phone,
                mail = row.mail,
                date_of_birth = row.date_of_birth,
                info = row.info,
                user_id = row.user_id,
                position_id = row.position_id)
        if not worker: 
            answer = dict(
                status="error",
                msg="Ошибка, работни не найден")
            return answer
        elif worker:
            set_cache("get_worker " + str(id), worker, cache["set"])
            answer = dict(
                status = "success",
                content = worker)
            return answer

@app.route('/get_workers_by_service',methods=['GET']) 
def get_workers_by_service(service_id = ''):
    logging('get_workers_by_service')
    if service_id == '':
        service_id = request.args.get('service_id')
    cache = get_cache("get_workers_by_service", service_id)
    try:
        rows = cache["get_workers_by_service " + service_id]
        answer = dict(
            status = "success",
            content = rows,
            count = len(rows))
        return answer
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        worker_q = session.query(Worker).join(Worker_services, Worker_services.worker_id == Worker.id).filter_by(service_id = service_id).all()
        workers = {}
        for row in worker_q:
            worker = dict(
                id = row.id,
                surname = row.surname,
                name = row.name,
                patronymic = row.patronymic,
                phone = row.phone,
                mail = row.mail,
                date_of_birth = row.date_of_birth,
                info = row.info,
                user_id = row.user_id,
                position_id = row.position_id)
            key = "worker " + str(row.id)
            workers[key] = worker
        if not workers: 
            answer = dict(
                status="error",
                msg="Врачи не найдены",
                count = 0)
            return answer
        elif workers:
            set_cache("get_workers_by_service " + str(service_id), workers, cache["set"])
            answer = dict(
                status = "success",
                content = workers,
                count = len(workers))
            return answer

#Filial##############################################################
@app.route('/get_filial',methods=['GET']) 
def get_filial(id = ''):
    logging('get_filial')
    if id == '':
        id = request.args.get('filial_id')

    cache = get_cache("get_filial", id)
    try:
        filial = cache["get_filial " + str(id)]
        answer = dict(
                status = "success",
                content = filial)
        return answer
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        filials_q = session.query(Filial).filter_by(id = id).all()
        filial = ''
        for row in filials_q:
            filial = dict(
                id = row.id,
                address_full = row.address_full,
                address = row.address,
                mail = row.mail)
        if not filial:
            answer = dict(
                status="error",
                msg="Филал не найден")
            return answer
        elif filial:
            set_cache("get_filial " + str(id), filial, cache["set"])
            answer = dict(
                status = "success",
                content = filial)
            return answer

@app.route('/get_all_filials',methods=['GET']) 
def get_all_filials():
    logging('get_all_filials')
    cache = get_cache("get_all_filials", 0)
    try:
        rows = cache["get_all_filials"]
        answer = dict(
            status = "success",
            content = rows,
            count = len(rows))
        return answer
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        filials_q = session.query(Filial).filter_by(visibility = True, date_of_delete = None).all()
        filials = {}
        for row in filials_q:
            filial = dict(
                id = row.id,
                address_full = row.address_full,
                address = row.address,
                mail = row.mail)
            key = "filial " + str(row.id)
            filials[key] = filial
        if not filials:
            answer = dict(
                status = "error",
                msg = "Врачи не найдены",
                count = 0)
            return answer
        elif filial:
            set_cache("get_all_filials", filials, cache["set"])
            answer = dict(
                status = "success",
                content = filials,
                count = len(filials))
            return answer

#PET#################################################################
@app.route('/get_pet',methods=['GET']) 
def get_pet(token = '', id = ''):
    if token == '':
        token = request.headers['token']
    if id == '':
        id = request.args.get('pet_id')

    logging('get_pet')
    try:
        client = get_client_by_user(token)['content']['client']
    except:
        answer = dict(
            status="error",
            msg="Ошибка авторизации")
        return answer
    Session = sessionmaker(bind=engine)
    session = Session()
    pet_q = session.query(Pet).filter_by(id = id).all()
    pet = {}
    for row in pet_q:
        kind = get_kind(row.kind_id)['content']
        pet = dict(
            id = row.id,
            name = row.name,
            sex = row.sex,
            date_of_birth = row.date_of_birth,
            kind = kind["value"],
            client = row.client_id)
    if not pet: 
        answer = dict(
            status = "error",
            msg = "Ошибка: питомец не найден")
        return answer
    elif pet["client"] != client["id"]:
        answer = dict(
            status = "error",
            msg = "Ошибка доступа: питомец не принадлежит клиенту")
        return answer
    elif pet["client"] == client["id"]:
        answer = dict(
            status = "success",
            content = pet)
        return answer

@app.route('/get_pets_by_client',methods=['GET'])
def get_pets_by_client():
    logging('get_pets_by_client')
    token = request.headers['token']
    Session = sessionmaker(bind=engine)
    session = Session()
    try:
        client = get_client_by_user(token)['content']['client']
    except:
        answer = dict(
            status="error",
            msg="Ошибка авторизации")
        return answer
    pet_q = session.query(Pet).filter_by(client_id = client["id"]).all()
    pets = {}
    for row in pet_q:
        kind = get_kind(row.kind_id)['content']
        pet = dict(
            id = row.id,
            name = row.name,
            sex = row.sex,
            date_of_birth = row.date_of_birth,
            kind = kind["value"],
            client = row.client_id)
        key = "pet " + str(row.id)
        pets[key] = pet
    answer = dict(
        status = "success",
        content = pets,
        count = len(pets))
    return answer

@app.route('/add_pet',methods=['POST'])
def add_pet():
    logging('add_pet')
    token = request.headers['token']
    pet_name = request.form['pet_name']
    pet_kind_id = request.form['pet_kind_id']
    pet_sex = request.form['pet_sex']

    try:
        client = get_client_by_user(token)['content']['client']
    except:
        answer = dict(
            status="error",
            msg="Ошибка авторизации")
        return answer
    try:
        # if not "date_of_birth" in pet:
        #     date_of_birth = None
        # if not "name" in pet:
        #     name = None
        # if not "photo" in pet:
        #     photo = None
        # if not "sex" in pet:
        #     sex = 1
        # if not "kind_id" in pet:
        #     kind_id = None
        insert_stmt = insert(Pet).values(
            name = pet_name,
            sex = pet_sex,
            # photo = photo,
            # date_of_birth = date_of_birth,
            kind_id = pet_kind_id,
            client_id = client['id'])
        conn = engine.connect()
        result = conn.execute(insert_stmt)
        pet_id = result.inserted_primary_key
        print("Добавлен питомец " + str(pet_id))
        answer = dict(
            status = "success",
            content = dict(id=str(pet_id)))
        return answer
    except:
        answer = dict(
            status = "error",
            msg = "Ошибка, птомец не создан")
        return answer

#KIND################################################################
@app.route('/get_kind',methods=['GET']) 
def get_kind(id = ''):
    logging('get_kind')
    if id == '':
        id = request.args.get('kind_id')
    cache = get_cache("get_kind", id)
    try:
        rows = cache["get_kind " + str(id)]
        answer = dict(
            status = "success",
            content = rows)
        return answer
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        kind_q = session.query(Kind).filter_by(id = id).all()
        kind = ''
        for row in kind_q:
            kind = dict(
                id = row.id,
                value = row.value)
        if not kind: 
            answer = dict(
                status="error",
                msg="Ошибка: вид питомца не найден")
            return answer
        elif kind:
            set_cache("get_kind " + str(id), kind, cache["set"])
            answer = dict(
                status="success",
                content=kind)
            return answer

@app.route('/get_all_kinds',methods=['GET'])
def get_all_kinds():
    logging('get_all_kinds')
    cache = get_cache("get_all_kinds", 0)
    try:
        rows = cache["get_all_kinds"]
        answer = dict(
            status = "success",
            content = rows,
            count = len(rows))
        return answer
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        kinds_q = session.query(Kind).all()
        kinds = {}
        for row in kinds_q:
            kind = dict(
                id = row.id,
                value = row.value)
            key = "kind " + str(row.id)
            kinds[key] = kind
        if not kinds:
            answer = dict(
                status = "error",
                msg = "Ошибка: виды питомцев не найдены",
                count = 0)
            return answer
        elif kind:
            set_cache("get_all_kinds", kinds, cache["set"])
            answer = dict(
                status = "success",
                content = kinds,
                count = len(kinds))
            return answer

#SERVICE#############################################################

@app.route('/get_all_services',methods=['GET'])
def get_all_services():
    logging('get_all_services')
    cache = get_cache("get_all_services", 0)
    try:
        rows = cache["get_all_services"]
        answer = dict(
            status = "success",
            content = rows,
            count = len(rows))
        return answer
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        services_q = session.query(Service).filter_by(visibility = True, date_of_delete = None).all()
        services = {}
        for row in services_q:
            service = dict(
                id = row.id,
                name = row.name,
                description = row.description,
                duration = row.duration,
                cost = row.cost,
                nurse = row.nurse)
            key = "service " + str(row.id)
            services[key] = service
        if not services:
            answer = dict(
                status = "error",
                msg = "Услуги не найдены",
                count = 0)
            return answer
        elif services:
            set_cache("get_all_services", services, cache["set"])
            answer = dict(
                status = "success",
                    content = services,
                    count = len(services))
            return answer

@app.route('/get_service',methods=['GET'])
def get_service(id = ''):
    logging('get_service')
    cache = get_cache("get_service", id)
    if id == '':
        id = request.args.get('service_id')
    try:
        rows = cache["get_service " + str(id)]
        answer = dict(
            status = "success",
            content = rows ,
            count = len(rows))
        return answer
    except:
        Session = sessionmaker(bind=engine)
        session = Session()
        service_q = session.query(Service).filter_by(id = id).all()
        service = ''
        for row in service_q:
            service = dict(
                id = row.id,
                name = row.name,
                description = row.description,
                duration = row.duration,
                cost = row.cost,
                nurse = row.nurse)
        if not service: 
            answer = dict(
                status = "error",
                msg = "Ошибка: услуга не найдена")
            return answer
        elif service:
            set_cache("get_service " + str(id), service, cache["set"])
            answer = dict(
                status = "success",
                content = service)
            return answer
 
#SCHEDULE############################################################

@app.route('/get_free_date_schedule',methods=['GET'])
def get_free_date_schedule(): 
    logging('get_free_date_schedule')
    filial_id = request.args.get('filial_id')
    worker_id = request.args.get('worker_id')

    dates = {}
    connect = pymysql.connect(
        host = '127.0.0.1',
        user = 'maria',
        db ='vc_2',
        password = '1234',
        charset ='utf8mb4')
    with connect:
        cur = connect.cursor()
        sql = """SELECT id, time1_start FROM schedule
        WHERE worker_id = %s and filial_id = %s and time1_start > CURDATE() """ % (worker_id, filial_id)
        cur.execute(sql)
        rows = cur.fetchall()
        for row in rows:
            date = row[1].date()
            dates[row[0]] = str(date)
    
    if len(dates) == 0:
        answer = dict(
            status = "error",
            msg = "Нет свободных дат",
            count = 0)
        return answer
    else:
        answer = dict(
            status = "success",
            content = dates,
            count = len(dates))
        return answer

@app.route('/get_free_time_schedule',methods=['GET'])
def get_free_time_schedule():# services - [id]
    logging('get_free_time_schedule')
    
    schedule_id = request.args.get('schedule_id')
    service_id = request.args.get('service_id')
    
    service = get_service(service_id)['content']
    duration = int(service["duration"])

    Session = sessionmaker(bind=engine)
    session = Session()
    schedules_q = session.query(Schedule).filter_by(id = schedule_id).all()

    for row in schedules_q:
        schedule = dict(
            id = row.id,
            worker_id = row.worker_id,
            # filial_id = row[2],
            # cabinet_id = row[3],
            time1_start = row.time1_start,
            time1_end = row.time1_end,
            time2_start = row.time2_start,
            time2_end = row.time2_end,
            time3_start = row.time3_start,
            time3_end = row.time3_end)
            # comment = row[10])
        date = row.time1_start.date()

    visits_q = session.query(Visit).filter(func.date(Visit.date) == date).all()
    print(visits_q)
    visits = {}
    for row in visits_q:
        date_end = row.date + datetime.timedelta(minutes = row.duration)
        visit = dict(
            doctor_id = row.doctor_id,
            date_start = row.date,
            date_end = date_end)
        key = row.id
        visits[key] = visit
    
    # dt = schedule["time1_start"] - datetime.timedelta(minutes = 20)  
    for visit in visits:
        for v in visits:
            if visits[visit]["date_start"] < visits[v]["date_start"]:
                vvv = visits[v]
                visits[v] = visits[visit]
                visits[visit] = vvv
    # duration
    times = {}
    time = {}
    dt = schedule["time1_start"]  
    i = 1
    
    for visit in visits:
        if visits[visit]["doctor_id"] == schedule["worker_id"]:
            while dt < schedule["time1_end"] :
                if dt >= visits[visit]["date_start"] and dt <= visits[visit]["date_end"]:
                    dt = visits[visit]["date_end"] + datetime.timedelta(minutes = 5) + datetime.timedelta(minutes = duration)
                    break
                else: 
                    time["date_end"] = str(dt) #end
                    time["date_start"] = str(dt - datetime.timedelta(minutes = duration))
                    times[i] = time.copy()
                    i += 1
                    dt = dt + datetime.timedelta(minutes = 5)


    while dt <= schedule["time1_end"]:
        dt = dt + datetime.timedelta(minutes = 5) + datetime.timedelta(minutes = duration)
        if dt <= schedule["time1_end"]:
            time["date_end"] = str(dt) #end
            time["date_start"] = str(dt - datetime.timedelta(minutes = duration))
            times[i] = time.copy()
            i += 1
    if len(times) == 0:
        answer = dict(
            status = "error",
            msg = "Нет свободных дат",
            count = 0)
        return answer
    else:
        answer = dict(
            status = "success",
            content = times,
            count = len(times))
        return answer

#CACHE###############################################################

def get_cache(request_name, id):
    data = ''
    answer = {}
    answer = dict(set = True)
    try:
        my_server = redis.Redis(connection_pool=POOL)
        if id == 0:
            data = my_server.get(request_name)
        else:
            data = my_server.get(request_name + " " + str(id))
    except:
        answer["set"] = False
        print("Ошибка подключения к Redis")
    if data != "" and data != None:
        print("Берем данные из кэша")
        if id == 0:
            answer[request_name] = json.loads(data)
        else:
            answer[request_name + ' ' + str(id)] = json.loads(data)

        answer["set"] = False

    return answer

def set_cache(request_name, answer, state_set):
    if state_set:
        try:
            my_server = redis.Redis(connection_pool=POOL)
            with my_server.pipeline() as pipe:
                pipe.set(request_name, bytes(json.dumps(answer), 'UTF-8'))
                pipe.execute()
                my_server.bgsave()
                print("Данные добавлены в кэш")
        except:
            print("(!)Ошибка подключения к Redis")
    else:
        print("(!)Данне не закэшированы")

#MUTEX###############################################################
class WinMutex:
    def __init__(self, name):
        self.mutexname = name
        self.mutex = win32event.CreateMutex(None, 1, self.mutexname)
        self.lasterror = win32api.GetLastError()

    def release(self):
        return win32event.ReleaseMutex(self.mutex)

    def wait_mutex(self, mutex):
        win32event.WaitForSingleObject(mutex, win32event.INFINITE)  

if __name__ == "__main__":
    app.run()
    mutex = WinMutex("mutex_vet_clinic")
    mutex.release()