import requests
from os import system
import json 

TOKEN = ''
URL = 'http://127.0.0.1:5000'

def start_client():
    while True:
        print("1 - Авторизация")
        print("2 - Регистрация")

        num = input()
        if not num.isdigit() or int(num) > 2: 
            print ("Неправильная команда!")
            continue
        num = int(num)
        msg = {}
        if num == 1:
            authorization()
        if num == 2:
            add_client()
            continue
        

def authorization():
    system('cls')
    while True:
        msg = {}
        print("АВТОРИЗАЦИЯ")
        print("Логин:")
        username = input()
        msg["login"] = username
        print("Пароль:")
        pwd = input()
        msg["password"] = pwd

        global URL
        url = URL + '/auth'
        response = requests.post(url, data=msg)
        answer = json.loads(response.text)
        if response.status_code == 200:
            if answer['status'] == 'error':
                print(answer['msg'])
                continue
            else:
                print("УСПЕХ")
                global TOKEN
                TOKEN = answer['content']['token']
                menu()
                break
        else:
            print("Ошибка: " + str(response.status_code))
            continue

def add_client():
    client = {}
    print("Логин:")
    client['login'] = input()
    while True:
        print("Пароль:")
        pass1 = input()
        print("Повторите пароль:")
        pass2 = input()

        if pass1 != pass2:
            print("Пароли не совпадают!")
            continue
        else:
            client['password'] = pass1
            global URL
            url = URL + '/add_client'
            response = requests.post(url, data=client)
            system('cls')
            answer = json.loads(response.text)
            if response.status_code == 200:
                if answer['status'] == 'error':
                    print(answer['msg'])
                else:
                    print("УСПЕХ")
                    break
            else:
                print("Ошибка: " + str(response.status_code))
                start_client()
            

def menu():
    while True:
        print("Главное меню")
        print("1 - Мои приемы")
        print("2 - Записаться на прием")
        print("3 - Отменить прием")
        print("4 - Мои питомцы")
        print("5 - Выйти")

        num = input()
        if not num.isdigit() or int(num) > 10: 
            print ("Неправильная команда!")
            continue
        num = int(num)
        msg = {}
        if num == 1:
            system('cls')
            get_visits()
        if num == 2:
            system('cls')
            add_visit()
        if num == 3:
            system('cls')
            cancel_visit()
        if num == 4:
            menu_pet()
            system('cls')
        if num == 5:
            clear_token()
            exit(0) 
 
def clear_token():
    global URL
    url = URL + '/clear_token'
    response = requests.post(url, headers={'token':TOKEN })

def cancel_visit():
    visits = get_visits()
    if(visits != -1 and visits != 0):
        while True:
            print("0 - назад")
            id = input("Введите id приема: ")
            if not id.isdigit(): 
                print ("Неправильная команда!")
                continue
            if id == "0":
                menu()
            if int(id) > 0:
                visit = "visit " + id
                if visits[visit]:
                    if visits[visit]["status"] == "актуальный":
                        global URL
                        url = URL + '/cancel_visit'
                        response = requests.post(url, params={'visit_id': id}, headers={'token':TOKEN })
                        answer = json.loads(response.text)
                        if response.status_code == 200:
                            if answer['status'] == 'success':
                                print('УСПЕХ')
                                menu()
                        else:
                            print("Ошибка: " + str(response.status_code))
                else:
                    print("Нет приема с таким номером")
                    continue
    else:
        print ("Нет приемов для отмены")
        menu()

def menu_pet():
    while True:
        print("Меню: ПИТОМЦЫ")
        print("1 - Показать моих питомцев")
        print("2 - Добавить питомца")
        print("3 - Показать приемы питомца")
        print("4 - Главное меню")
        num = input()
        if not num.isdigit() or int(num) > 6: 
            print ("Неправильная команда!")
            continue
        num = int(num)
        msg = {}
        if num == 1:
            system('cls')
            get_pets_by_client()
        elif num == 2:
            system('cls')
            add_pet()
        elif num == 3:
            system('cls')
            get_visit_by_pet()
        elif num == 4:
            menu()
        else:
            print ("Неправильная команда!")
            continue
        menu()

def get_pets_by_client():
    global URL
    url = URL + '/get_pets_by_client'
    response = requests.get(url, headers={'token':TOKEN })
    answer = json.loads(response.text)
    if response.status_code == 200:
        if answer['status'] == 'error':
            print(answer['msg'])
            return -1
        if answer['status'] == 'error' and answer['count'] == 0:
            print("Питомцы не найдены")
            return 0
        pets = answer['content']
        print_pets(pets)
        return pets
    else:
        print("Ошибка: " + str(response.status_code))
        menu()


def print_pets(pets):
    sex_pet = "ж"
    for pet in pets:
        if pets[pet]["sex"] == 1:
            sex_pet = "м"
        else:
            sex_pet = "ж"
        print ("%s - %s - %s - %s - %s" % (
            pets[pet]["id"], 
            pets[pet]["name"],
            sex_pet,
            pets[pet]["kind"],
            pets[pet]["date_of_birth"]))
    print()

def get_visits():
    global URL
    url = URL + '/get_visit_by_client'
    response = requests.get(url, headers={'token':TOKEN })
    answer = json.loads(response.text)
    if response.status_code == 200:
        if answer['status'] == 'error':
            print(answer['msg'])
            return -1
        if answer['status'] == 'success' and answer['count'] == '0':
            print("Приемы не найдены")
            return 0
        if answer['status'] == 'success' and answer['count'] > 0:
            visits = answer['content']
            print_visits(visits)
            return visits
    else:
        print("Ошибка сервера: " + str(response.status_code))

def print_visits(visits):
    system('cls')
    for visit in visits:
        print ("%s - %s - %s - %s - %s - %s - %s - %s" % (
            visits[visit]["id"], 
            visits[visit]["datetime"],
            visits[visit]["status"],
            visits[visit]["cabinet"],
            visits[visit]["doctor"],
            visits[visit]["filial"],
            visits[visit]["pet"],
            visits[visit]["duration"]))
        print()
    print()

def select_kind():
    global URL
    url = URL + '/get_all_kinds'
    response = requests.get(url)
    answer = json.loads(response.text)
    if response.status_code == 200:
        if answer['count'] == 0:
            print("Виды животных не найдены")
            return 0
            menu()
        if answer['count'] > 0:
            kinds = answer['content']
            sel_kind = print_kinds(kinds)
            return sel_kind
    else:
        print("Ошибка: " + str(response.status_code))

def print_kinds(kinds):
    while True:
        print()
        print("Выберите вид животного из списка:")
        for id in kinds.keys():
            print ("%s - %s" % (
                kinds[id]["id"], 
                kinds[id]["value"]))
        kind = input()
        k = "kind " + kind
        if kinds[k]:
            return kind
        else:
            continue

def add_pet():
    print("Создание питомца")
    print("Кличка:")
    pet_name = input()
    print()
    pet_sex = 0
    while True:
        print("Пол (0 - ж, 1 - м):")
        s = input()
        if s == '0' or s == '1':
            pet_sex = s
            break
        else:
            continue
    
    pet_kind_id = select_kind()
    
    global URL
    url = URL + '/add_pet'
    response = requests.post(url, data={
        'pet_name':pet_name, 
        'pet_kind_id':pet_kind_id,
        'pet_sex':pet_sex},
        headers={'token':TOKEN })
    answer = json.loads(response.text)
    if response.status_code == 200:
        if answer['status'] == 'success':
            print("УСПЕХ")
        elif answer['status'] == 'error':
            print(answer['msg'])
        menu()
    else:
        print("Ошибка сервера: " + str(response.status_code))

    # return pet

def get_visit_by_pet():
    pets = get_pets_by_client()
    if len(pets) > 0:
        while True:
            print("0 - назад")
            pet_id = input("Введите id питомца: ")
            if not pet_id.isdigit(): 
                print ("Неправильная команда!")
                continue
            if pet_id == "0":
                menu()
            global URL
            url = URL + '/get_visit_by_pet'
            response = requests.get(url, params={'pet_id':pet_id}, headers={'token':TOKEN })
            answer = json.loads(response.text)
            if response.status_code == 200:
                if answer['status'] == 'success' and answer['count'] == 0:
                    print("Приемы не найдены")
                    return 0
                if answer['status'] == 'success' and answer['count'] > 0:
                    visits = answer['content']
                    print_visits(visits)
                    return visits
            else:
                print("Ошибка: " + str(response.status_code))
    else:
        print ("У вас нет зарегистрированных питомцев")

def get_all_services():
    global URL
    url = URL + '/get_all_services'
    response = requests.get(url)
    answer = json.loads(response.text)
    if response.status_code == 200:
        if answer['count'] == 0:
            print("Услуги не найдены")
            return 0
        if answer['count'] > 0:
            services = answer['content']
            print_services(services)
            return services
    else:
        print("Ошибка: " + str(response.status_code))
        menu()

def print_services(services):
    for id in services.keys():
        print ("%s - %s - %s - %s - %s" % (
            services[id]["id"], 
            services[id]["name"], 
            services[id]["description"],
            services[id]["duration"],
            services[id]["cost"]))
    print()

def get_all_filials():
    global URL
    url = URL + '/get_all_filials'
    response = requests.get(url)
    answer = json.loads(response.text)
    if response.status_code == 200:
        if answer['count'] == 0:
            print("Филиалы не найдены")
            return 0
        filials = answer['content']
        print_filials(filials)
        return filials
    else:
        print("Ошибка: " + str(response.status_code))

def print_filials(filials):
    for id in filials.keys():
        print ("%s - %s - %s - %s" % (
            filials[id]["id"], 
            filials[id]["address_full"], 
            filials[id]["address"],
            filials[id]["mail"]))
    print()

def get_workers_by_service(service_id):
    global URL
    url = URL + '/get_workers_by_service'
    response = requests.get(url, params={'service_id':service_id})
    answer = json.loads(response.text)
    if response.status_code == 200:
        if answer['count'] == 0:
            print("Врачи не найдены")
            return 0
        workers = answer['content']
        print_workers(workers)
        return workers
    else:
        print("Ошибка: " + str(response.status_code))
        menu()

def print_workers(workers):
    for id in workers.keys():
        print ("%s - %s - %s - %s - %s - %s - %s - %s" % (
            workers[id]["id"], 
            workers[id]["surname"],
            workers[id]["name"],
            workers[id]["patronymic"],
            workers[id]["phone"],
            workers[id]["mail"],
            workers[id]["date_of_birth"],
            workers[id]["info"]))
    print()

def get_free_date_schedule(filial_id, worker_id): 
    global URL
    url = URL + '/get_free_date_schedule'
    response = requests.get(url, params={'filial_id':filial_id, 'worker_id':worker_id})
    answer = json.loads(response.text)
    if response.status_code == 200:
        if answer['count'] == 0:
            print("Даты не найдены")
            return 0
        if answer['count'] > 0:
            dates = answer['content']
            print_dates(dates)
            return dates
    else:
        print("Ошибка: " + str(response.status_code))

def print_dates(dates):
    for id in dates.keys():
        print("%s - %s" % (id, dates[id]))
        print()


def get_free_time_schedule(schedule_id, service_id):
    global URL
    url = URL + '/get_free_time_schedule'
    response = requests.get(url, params={'schedule_id':schedule_id, 'service_id':service_id})
    answer = json.loads(response.text)
    if response.status_code == 200:
        if answer['count'] == 0:
            print("Свободное время не найдено")
            return 0
        if answer['count'] > 0:
            times = answer['content']
            print_times(times)
            return times
    else:
        print("Ошибка: " + str(response.status_code))

def print_times(times):
    for id in times.keys():
        print("%s - %s" % (id, times[id]["date_start"]))
    print()


def add_visit():
    system('cls')
    print("Запись на прием")
    pet_id = ''
    service_id = ''
    filial_id = ''
    doctor_id = ''
    date = ''
    schedule_id = ''
#питомец
    print("1. Выбор питомца")
    pets = get_pets_by_client()
    if len(pets) > 0:
        while True:
            print("0 - назад")
            pet_id = input("Введите id питомца: ")
            if not pet_id.isdigit(): 
                print ("Неправильная команда!")
                continue
            if pet_id == "0":
                menu()
            k = 0
            for pet in pets:
                if int(pet_id) == pets[pet]['id']:
                    k = k + 1
                    break
            if k == 0:
                print ("Выберите номер питомца из списка!")
                continue
            break
    else:
        system('cls')
        print ("У вас нет зарегистрированных питомцев")
        menu()
#услуга
    system('cls')
    print("2. Выбор услуги")
    services = get_all_services()
    if services != 0 and services != -1:
        print()
        while True:
            print("0 - меню")
            service_id = input("Введите id услуги: ")
            if not service_id.isdigit(): 
                print ("Неправильная команда!")
                continue
            if service_id == "0":
                menu()
            k = 0
            for service in services:
                if int(service_id) == services[service]['id']:
                    k = k + 1
                    break
            if k == 0:
                print ("Выберите номер услуги из списка!")
                continue
            break
    else:
        system('cls')
        print ("Нет услуг для записи")
        menu()
    system('cls')
#филиал
    system('cls')
    print("3. Выбор филиала")
    filials = get_all_filials()
    if filials != 0 and filials != -1:
        print()
        while True:
            print("0 - меню")
            filial_id = input("Введите id филилала: ")
            if not filial_id.isdigit(): 
                print ("Неправильная команда!")
                continue
            if filial_id == "0":
                menu()

            k = 0
            for filial in filials:
                if int(filial_id) == filials[filial]['id']:
                    k = k + 1
                    break
            if k == 0:
                print ("Выберите номер филила из списка!")
                continue
            break
    else:
        print ("Нет филиала для выбора")
        menu()
#врач
    system('cls')
    print("4. Выбор врача")
    workers = get_workers_by_service(service_id)
    if workers != 0 and workers != -1:
        print()
        while True:
            print("0 - меню")
            worker_id = input("Введите id врача: ")
            if not worker_id.isdigit(): 
                print ("Неправильная команда!")
                continue
            if worker_id == "0":
                menu()

            k = 0
            for worker in workers:
                if int(worker_id) == workers[worker]['id']:
                    k = k + 1
                    break
            if k == 0:
                print ("Выберите номер врача из списка!")
                continue
            break
    else:
        print ("Нет врача для выбора")
        menu()
#даты
    system('cls')
    print("5. Выбор даты")
    dates = get_free_date_schedule(filial_id, worker_id)
    if dates != 0 and dates != -1:
        while True:
            print("0 - меню")
            schedule_id = input("Введите id даты: ")
            if not schedule_id.isdigit(): 
                print ("Неправильная команда!")
                continue
            if schedule_id == "0":
                menu()
            k = 0
            for date in dates:
                if schedule_id == date:
                    k = k + 1
                    break
            if k == 0:
                print ("Выберите номер даты из списка!")
                continue
            system('cls')
            break
    else:
        print ("Нет дат для выбора")
        menu()
#время
    time = ''
    system('cls')
    print("6. Выбор времени")
    times = get_free_time_schedule(schedule_id, service_id)
    if times != 0 and times != -1 and times != None:
        print()
        while True:
            print("0 - меню")
            time_id = input("Введите id времени: ")
            if not time_id.isdigit(): 
                print ("Неправильная команда!")
                continue
            if time_id == "0":
                menu()
            k = 0
            for time in times:
                if time_id == time:
                    k = k + 1
                    break
            if k == 0:
                print ("Выберите номер времени из списка!")
                continue
            time = times[time_id]['date_start']
            break
    else:
        print ("Нет времени для выбора")
        menu()

    global URL
    url = URL + '/add_visit'
    response = requests.post(url, data={
        'pet_id':pet_id,
        'date':time,
        'worker_id':worker_id, 
        'filial_id':filial_id,
        'service_id':service_id},
        headers={'token':TOKEN })
    if response.status_code == 200:
        answer = json.loads(response.text)
        if answer['status'] == 'error':
            print(answer['msg'])
            return -1
        elif answer['status'] == 'success':
            print('УСПЕХ')

    else:
        print("Ошибка: " + str(response.status_code))









start_client()
